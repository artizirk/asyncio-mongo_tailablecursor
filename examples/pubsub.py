#!/usr/bin/env python3
# a very ugly hack
import asyncio
import asyncio_mongo as mongodb
import datetime
import sys

@asyncio.coroutine
def testfunc():
    mongo = yield from mongodb.Connection.create('localhost', 27017)

    db = mongo.test
    c = yield from db.create_collection("pubsub", {"capped":True, "size":100000, "autoIndexId":False})

@asyncio.coroutine
def insert_some_doc():
    mongo = yield from mongodb.Connection.create("localhost", 27017)

    c = mongo.test.pubsub
    for x in range(3):
        r = yield from c.insert({"message": "nr: {}".format(x),
                                    "datetime": datetime.datetime.now(),})
        print(r)

@asyncio.coroutine
def sub_test():
    mongo = yield from mongodb.Connection.create()
    c = mongo.test.pubsub
    cursor, docs = yield from c.find(tailable=True, await_data=True)
    while True:
        for x in docs:
            print(x)
        cursor, docs = yield from cursor
        #yield from asyncio.sleep(1)


if __name__ == '__main__':
    f = sub_test()
    if "push" in sys.argv:
        f = insert_some_doc()
    asyncio.get_event_loop().run_until_complete(f)

